import json
from lista_osob import lista_osob

class Lista_obecnosci:
    lista_obecnych = []
    lista_nieobecnych = []
    dziennik = {}


    def __init__(self, name):
        self.name = name

    def drukuj(self):
        print(f'Obecni dnia {self.name} :')
        for pos, item in enumerate(self.lista_obecnych):
            print(f'{pos}. {item}')
        print(f'Nieobecni dnia {self.name} :')
        for pos, item in enumerate(self.lista_nieobecnych):
            print(f'{pos}. {item}')

    def sprawdz_obecnosc(self):
        with open('data.json') as json_file:
            self.dziennik = json.load(json_file)
        print('Sprawdzamy obecnosc.. wybieraj t/n')
        for item in lista_osob:
            answ = input(f'{item} jest? ')
            if answ == 't':
                self.lista_obecnych.append(item)
                temp = self.dziennik[item]
                temp.append(self.name)
                self.dziennik[item] = temp
            else:
                self.lista_nieobecnych.append(item)
                temp = self.dziennik[item]
                temp.append('---')
                self.dziennik[item] = temp

        with open('data.json', 'w') as outfile:
            json.dump(self.dziennik, outfile)

    def absencja(self):
        with open('data.json') as json_file:
            self.dziennik = json.load(json_file)

        for item in self.dziennik:
            temp = self.dziennik[item]
            print(f'{item}: ')
            for foo in temp:
                if foo == '---':
                    print('X',end='')
                else:
                    print('_',end='')
            print('\n')

    def tworz_plik_dziennika(self):
        print('ZOSTANIE UTORZONY NOWY PLIK DZIENNIKA!')
        print('DOTYCHCZASOWE DANE ZOSTANĄ UTRACONE!')
        foo = str(input('KONTYNULOWAC? (t/n)'))
        if foo == 'n':
            return None
        for item in lista_osob:
            self.dziennik[item] = []
        print(self.dziennik)
        with open('data.json', 'w') as outfile:
            json.dump(self.dziennik, outfile)