from datetime import date
import lista_obecnosci

class Lista_baza:

    def __init__(self):
        self.lista_ob = lista_obecnosci.Lista_obecnosci(str(date.today()))
        self.start()


    def start(self):

        menu = ['MENU:', 'Sprawdz obecnosc', 'Pokaż dziennik - cały', 'Pokaż dziennik - dzisiaj', 'Konczymy!',
                'Twórz plik dziennika']

        while(True):
            print(30 * '=')
            for pos, item in enumerate(menu):
                print(f'{pos}. {item}')
            answer = input(f'Wybierz ')
            if answer == '1':
                self.lista_ob.sprawdz_obecnosc()
            elif answer == '2':
                self.lista_ob.absencja()
            elif answer == '3':
                self.lista_ob.drukuj()
            elif answer == '4':
                break
            elif answer == '5':
                self.lista_ob.tworz_plik_dziennika()


